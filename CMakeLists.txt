cmake_minimum_required(VERSION 3.10)

set(APP "dnsbalancer")

project(${APP} C ASM)

set(CMAKE_MODULE_PATH ${PROJECT_SOURCE_DIR}/cmake-scripts)
include(generic)

add_subdirectory(contrib)
add_subdirectory(core)

file(WRITE empty.c "")

add_executable(${APP}
	empty.c)

target_link_libraries(${APP}
	core
	avl
	config
	ldns
	pfcq
	unwind)

if (NOT "${DS_LINK_MODE}" STREQUAL "static")
	target_link_libraries(${APP}
		pthread)
endif()

install(TARGETS dnsbalancer
	RUNTIME DESTINATION bin)

