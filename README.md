[![pipeline status](https://gitlab.com/post-factum/dnsbalancer/badges/master/pipeline.svg)](https://gitlab.com/post-factum/dnsbalancer/commits/master)

dnsbalancer
===========

Description
-----------

Daemon to balance UDP DNS requests over DNS servers.

Principles
----------

TBD.

Building
--------

### Prerequisites

* `Linux kernel` v4.5+ (with `SO_REUSEPORT` and `EPOLLEXCLUSIVE` support, tested with v4.15.10)
* `binutils` (tested with v2.29.1)
* `clang` (tested with v5.0.1) or `gcc` (tested with v7.3.0)
* `musl` (tested with v1.1.19) or `glibc` (tested with v2.26)
* `lld` (is used with `clang` only, tested with v5.0.1)
* `cmake` (tested with v3.10.2)
* `ninja` (tested with v1.8.2) or `make` (tested with v4.2.1)

### Compiling

Create `build` folder and chdir into it.

Type `CC=clang cmake -G Ninja -DDS_LINK_MODE=static -DCMAKE_BUILD_TYPE=Release ..` to prepare a build environment.

Type `cmake --build .` to generate a static build for production use. Static build is linked against `musl`.

`gcc` is supported too.

Omit setting `DS_LINK_MODE` to generate a dynamic executable, linked against `glibc`.

Set `CMAKE_BUILD_TYPE` to `Debug` to generate a debug build.

Another generator may be used with the `-G` option.

Configuration
-------------

`dnsbalancer` uses `libconfig`-style configuration file. An example of default configuration is provided in the `dnsbalancer.conf.sample` file.

The configuration file consists of 5 chapters:

* `general` where the common settings are set up
* `stats` (not used at the moment) where the settings for the internal statistics routine are set up
* `frontends` where the frontends and the list of corresponding action sets are configured
* `actions` where the request processing rules are defined
* `forwarders` where the forwarder and its watchdog settings are written

Details on each section are given below.

### `general`

The following settings apply:

* `wrks` defines an amount of worker threads to spawn; by default it is `-1` meaning each CPU will get its own thread
* `max_pkt_size` sets the maximum size of DNS request; bigger requests will be discarded; by default it is `512`
* `req_ttl` (in msecs) sets the timeout for the request to be processed by forwarder; if the request is not satisfied within this time frame, it gets garbage collected; the default value is `5000`
* `gc_intvl` (in msecs) defines how often the garbage collector will be invoked; it is `1000` by default
* `wdt_intvl` (in msecs) sets the frequency of polling the forwarders to check whether they are alive; by default, it is `1000`
* `poll_timeo` (in msecs) specifies the timeout for the event loop on worker threads reload while waiting for pending requests; it is `100` by default, and do not change it unless you know what you are doing
* `tk_intvl` (in msecs) defines timekeeping granularity (the size of the epoch); the default value is `1000`, and this also should not be changed without a strong reason

### `stats`

* `addr` defines an IP address to listen for HTTP requests to
* `port` defines a TCP port for listening.

This section is not currently used, and it might be renamed to reflect planned RPC implementation.

### `frontends`

This is an array. Each element is specified as follows:

* `name` sets custom frontend name
* `addr` defines an IP address to listen for incoming UDP DNS requests
* `port` specifies a UDP port; it is `53` by default
* `dscp` sets IP QoS DiffServ option; it is `0x12` by default
* `actions` lists the sets of actions that are applied to each request

### `actions`

This is an array. Each element is specified as follows:

* `name` specifies custom action name
* `rules` lists multiple rules that manipulate the request

Each rule has the following options:

* `name` sets custom rule name
* `net` matches a request by the source subnet
* `mask` accompanies `net` with the network mask
* `type` matches a request by a DNS RR type; `*` is used for any type
* `class` matches a request by a DNS RR class; `*` is used for any class
* `fqdn` matches a request by the FQDN in the question section; it is a regex; mind a trailing dot to match an FQDN strictly
* `do` specifies what action to perform on the request

Actions are defined as follows:

* `what` sets what to do; can be `balance` to distribute requests over the specified set of forwarders or `drop` to drop the request silently

In case `balance` action is specified:

* `how` specifies distribution algorithm; can be `rr` for round-robin distribution (better for fairness) or `sticky` to do a hash-based distribution (better for forwarder caches)
* `where` specifies the list of forwarders

### `forwarders`

This is an array. Each element is specified as follows:

* `name` gives a forwarder custom name
* `addr` sets forwarder IP address
* `port` sets forwarder UDP port; it is `53` by default
* `weight` sets forwarder weight for round-robin distribution (not used at the moment)
* `dscp` sets IP QoS DiffServ option; it is `0x12` by default
* `wdt` specifies forwarder watchdog options

Watchdog options are:

* `dscp` (see above)
* `tries` defines how many attempts will be made to reach the forwarder before it is considered to be down
* `query` specifies watchdog query

Usage
-----

Typical command line:

`dnsbalancer --config=/etc/dnsbalancer/dnsbalancer.conf --verbose --syslog`

Supported arguments:

* `--config=<path>` (mandatory) specifies configuration file to use
* `--daemonize` (optional) enables daemonisation (preferred way to run on server)
* `--verbose` (optional) enables verbose output
* `--debug` (optional) enables debug output (for debug build only, otherwise does nothing)
* `--syslog` (optional) logs everything to syslog instead of `/dev/stderr`
* `--help` (optional) prints short help
* `--version` (optional) shows application version

TODO and to investigate
-----------------------

* `-O1 -D_FORTIFY_SOURCE=2` for `glibc` debug builds
* `-D_FORTIFY_SOURCE=1` for `glibc` release builds
* profiling with `-pg`?
* actions: SET A, reject with NXDOMAIN
* weighted RR distribution
* HTTP stats ([mongoose](https://github.com/cesanta/mongoose))
* JSON stats over HTTP
* check and match watchdog responses?
* temporary ACLs with TTL
* HTTP RPC (e.g., for manipulating temporary ACLs)
* servfail/nxdomain log
* IP logging for set of FQDNs
* TCP?
* [RFC7871](https://tools.ietf.org/html/rfc7871)

Distribution and Contribution
-----------------------------

Distributed under terms and conditions of GNU GPL v3 (only).

Contribs:

* `ldns`, 3-Clause BSD
* `libavl` RB-tree implementation, GPLv2 or later
* `libconfig`, LGPLv2.1 or later
* `libunwind`, MIT
* `pfcq`, GPLv3 only
* `queue.h`, 3-Clause BSD

Developers:

* Oleksandr Natalenko &lt;oleksandr@natalenko.name&gt;

Contributors:

* Andriĵ Kostenecjkyĵ (initial testing, core suggestions)
* Anton Baranov (statistics suggestions)
* Oleg Matviichuk &lt;ma7hway@gmail.com&gt; (exit routines, help messages)
* Oleksiĵ Velyčko (TODO suggestions)
* Serhiĵ Okunj (testing, core suggestions)

