add_library(config STATIC
	grammar.c
	libconfig.c
	scanctx.c
	scanner.c
	strbuf.c
	strvec.c
	util.c
	wincompat.c)

add_definitions(-DHAVE_CONFIG_H)

