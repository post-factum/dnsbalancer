/* vim: set tabstop=4:softtabstop=4:shiftwidth=4:noexpandtab */

/*
 * dnsbalancer - daemon to balance UDP DNS requests over DNS servers
 * Initially created under patronage of Lanet Network
 * Programmed by Oleksandr Natalenko <oleksandr@natalenko.name>, 2015-2018
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "pkt.h"

#include "act.h"

bool ds_act_match(struct ds_wrk_tsk* _tsk, struct ds_act_item* _act_item)
{
	if (_act_item->rr_class != ds_pkt_get_q_class(&_tsk->pkt) && _act_item->rr_class != DS_RR_CLASS_ALL)
		return false;

	if (_act_item->rr_type != ds_pkt_get_q_type(&_tsk->pkt) && _act_item->rr_type != DS_RR_TYPE_ALL)
		return false;

	if (!pfcq_net_addr_in_subnet(&_tsk->addr, &_act_item->addr, &_act_item->mask))
		return false;

	if (regexec(&_act_item->regex, _tsk->pkt.fqdn, 0, NULL, 0) !=0)
		return false;

	return true;
}

