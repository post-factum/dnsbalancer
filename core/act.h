/* vim: set tabstop=4:softtabstop=4:shiftwidth=4:noexpandtab */

/*
 * dnsbalancer - daemon to balance UDP DNS requests over DNS servers
 * Initially created under patronage of Lanet Network
 * Programmed by Oleksandr Natalenko <oleksandr@natalenko.name>, 2015-2018
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <regex.h>

#include "../contrib/pfcq/pfcq.h"
#include "wrk.h"

enum ds_act_type
{
	DS_ACT_UNK = 0,
	DS_ACT_BALANCE,
	DS_ACT_DROP,
	DS_ACT_REJECT,
};

enum ds_act_balance_type
{
	DS_ACT_BALANCE_UNK = 0,
	DS_ACT_BALANCE_RR,
	DS_ACT_BALANCE_STICKY,
};

enum ds_act_reject_type
{
	DS_ACT_REJECT_UNK = 0,
	DS_ACT_REJECT_NXDOMAIN,
	DS_ACT_REJECT_SERVFAIL,
};

struct ds_act_balance_struct
{
	enum ds_act_balance_type type;
	size_t nfwds;
	struct ds_fwd** fwds;
	struct pfcq_counter c_fwd;
};

struct ds_act_reject_struct
{
	enum ds_act_reject_type type;
};

struct ds_act_struct
{
	enum ds_act_type type;
	union params {
		struct ds_act_balance_struct balance;
		struct ds_act_reject_struct reject;
	} params;
};

struct ds_act_item
{
	const char* name;
	struct pfcq_net_addr addr;
	struct pfcq_net_addr mask;
	ds_rr_class_t rr_class;
	ds_rr_type_t rr_type;
	const char* expr;
	regex_t regex;
	struct ds_act_struct act;
};

struct ds_act
{
	const char* name;
	size_t nact_items;
	struct ds_act_item* act_items;
};

bool ds_act_match(struct ds_wrk_tsk*, struct ds_act_item*) __attribute__((warn_unused_result));
