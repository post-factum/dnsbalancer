/* vim: set tabstop=4:softtabstop=4:shiftwidth=4:noexpandtab */

/*
 * dnsbalancer - daemon to balance UDP DNS requests over DNS servers
 * Initially created under patronage of Lanet Network
 * Programmed by Oleksandr Natalenko <oleksandr@natalenko.name>, 2015-2018
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../contrib/pfcq/pfcq.h"

#include "cfg.h"

ds_cfg_t* ds_cfg_open(const char* _filepath)
{
	ds_cfg_t* ret = NULL;

	ret = pfcq_alloc(sizeof(ds_cfg_t));
	config_init(ret);
	if (likely(config_read_file(ret, _filepath) == CONFIG_TRUE))
		return ret;

	inform("%s: %s, line %d\n",
		config_error_file(ret),
		config_error_text(ret),
		config_error_line(ret));
	stop(NULL);
}

ds_cfg_node_t* ds_cfg_node_get_root(const ds_cfg_t* _cfg)
{
	return config_root_setting(_cfg);
}

ds_cfg_node_t* ds_cfg_node_get_subnode(const ds_cfg_node_t* _node, const char* _name)
{
	ds_cfg_node_t* ret = NULL;

	ret = config_setting_get_member(_node, _name);
	if (likely(ret))
		return ret;

	inform("Unable to get a subnode: %s\n", _name);
	stop(NULL);
}

size_t ds_cfg_get_node_len(const ds_cfg_node_t* _node)
{
	return config_setting_length(_node);
}

ds_cfg_node_t* ds_cfg_get_node_elem(const ds_cfg_node_t* _node, const size_t _elem)
{
	return config_setting_get_elem(_node, _elem);
}

uint64_t ds_cfg_node_get_u64(const ds_cfg_node_t* _node, const char* _name, const bool _optional, const uint64_t _default)
{
	ds_cfg_node_t* subnode = NULL;
	long long ret = 0;

	subnode = config_setting_get_member(_node, _name);
	if (likely(subnode))
	{
		ret = config_setting_get_int64(subnode);
		if (ret >= 0)
			return (uint64_t)ret;

		inform("u64 value \"%s\" at line %d is negative", _name, config_setting_source_line(subnode));
	} else
		inform("Unable to get u64 value \"%s\" under line %d", _name, config_setting_source_line(_node));

	if (_optional)
	{
		verbose(", defaulting to %zu\n", _default);
		return _default;
	}

	stop(", the value is mandatory.\n");
}

int64_t ds_cfg_node_get_i64(const ds_cfg_node_t* _node, const char* _name, const bool _optional, const int64_t _default)
{
	ds_cfg_node_t* subnode = NULL;

	subnode = config_setting_get_member(_node, _name);
	if (likely(subnode))
	{
		return config_setting_get_int64(subnode);
	} else
		inform("Unable to get i64 value \"%s\" under line %d", _name, config_setting_source_line(_node));

	if (_optional)
	{
		verbose(", defaulting to %zu\n", _default);
		return _default;
	}

	stop(", the value is mandatory.\n");
}

const char* ds_cfg_node_get_str(const ds_cfg_node_t* _node, const char* _name)
{
	ds_cfg_node_t* subnode = NULL;
	const char* ret = NULL;

	subnode = config_setting_get_member(_node, _name);

	if (likely(subnode))
		ret = config_setting_get_string(subnode);

	if (likely(ret))
		return ret;

	inform("Unable to get string value \"%s\" under line %d\n", _name, config_setting_source_line(_node));

	stop(NULL);
}

const char* ds_cfg_node_get_self_str(ds_cfg_node_t* _set)
{
	return config_setting_get_string(_set);
}

void ds_cfg_close(ds_cfg_t* _config)
{
	config_destroy(_config);
	pfcq_free(_config);

	return;
}

