/* vim: set tabstop=4:softtabstop=4:shiftwidth=4:noexpandtab */

/*
 * dnsbalancer - daemon to balance UDP DNS requests over DNS servers
 * Initially created under patronage of Lanet Network
 * Programmed by Oleksandr Natalenko <oleksandr@natalenko.name>, 2015-2018
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <stdbool.h>
#include <stdint.h>

#include "../contrib/libconfig/libconfig.h"

#define DS_CFG_KEY_GENERAL			"general"
#define DS_CFG_KEY_STATS			"stats"
#define DS_CFG_KEY_FORWARDERS		"forwarders"
#define DS_CFG_KEY_ACTIONS			"actions"
#define DS_CFG_KEY_FRONTENDS		"frontends"
#define DS_CFG_KEY_NAME				"name"
#define DS_CFG_KEY_ADDR				"addr"
#define DS_CFG_KEY_PORT				"port"
#define DS_CFG_KEY_DSCP				"dscp"
#define DS_CFG_KEY_WDT				"wdt"
#define DS_CFG_KEY_CLASS			"class"
#define DS_CFG_KEY_TYPE				"type"
#define DS_CFG_KEY_FQDN				"fqdn"
#define DS_CFG_KEY_QUERY			"query"
#define DS_CFG_KEY_TRIES			"tries"
#define DS_CFG_KEY_RULES			"rules"
#define DS_CFG_KEY_NET				"net"
#define DS_CFG_KEY_MASK				"mask"
#define DS_CFG_KEY_DO				"do"
#define DS_CFG_KEY_WHAT				"what"
#define DS_CFG_KEY_BALANCE			"balance"
#define DS_CFG_KEY_HOW				"how"
#define DS_CFG_KEY_RR				"rr"
#define DS_CFG_KEY_STICKY			"sticky"
#define DS_CFG_KEY_WHERE			"where"
#define DS_CFG_KEY_DROP				"drop"
#define DS_CFG_KEY_REJECT			"reject"
#define DS_CFG_KEY_WITH				"with"
#define DS_CFG_KEY_NXDOMAIN			"nxdomain"
#define DS_CFG_KEY_SERVFAIL			"servfail"
#define DS_CFG_KEY_WRKS				"wrks"
#define DS_CFG_KEY_REQ_TTL			"req_ttl"
#define DS_CFG_KEY_GC_INTVL			"gc_intvl"
#define DS_CFG_KEY_WDT_INTVL		"wdt_intvl"
#define DS_CFG_KEY_POLL_TIMEO		"poll_timeo"
#define DS_CFG_KEY_MAX_PKT_SIZE		"max_pkt_size"
#define DS_CFG_KEY_TK_INTVL			"tk_intvl"

#define DS_CFG_DEFAULT_DNS_PORT		53
#define DS_CFG_DEFAULT_DSCP			0x12
#define DS_CFG_DEFAULT_TRIES		3
#define DS_CFG_DEFAULT_WRKS			(-1)
#define DS_CFG_DEFAULT_REQ_TTL		5000
#define DS_CFG_DEFAULT_GC_INTVL		1000
#define DS_CFG_DEFAULT_WDT_INTVL	1000
#define DS_CFG_DEFAULT_POLL_TIMEO	100
#define	DS_CFG_DEFAULT_MAX_PKT_SIZE	512
#define DS_CFG_DEFAULT_TK_INTVL		1000

typedef config_t ds_cfg_t;
typedef config_setting_t ds_cfg_node_t;

ds_cfg_t* ds_cfg_open(const char*) __attribute__((warn_unused_result));
ds_cfg_node_t* ds_cfg_node_get_root(const ds_cfg_t*) __attribute__((warn_unused_result));
ds_cfg_node_t* ds_cfg_node_get_subnode(const ds_cfg_node_t*, const char*) __attribute__((warn_unused_result));
size_t ds_cfg_get_node_len(const ds_cfg_node_t*) __attribute__((warn_unused_result));
ds_cfg_node_t* ds_cfg_get_node_elem(const ds_cfg_node_t*, const size_t) __attribute__((warn_unused_result));
uint64_t ds_cfg_node_get_u64(const ds_cfg_node_t*, const char*, const bool, const uint64_t) __attribute__((warn_unused_result));
int64_t ds_cfg_node_get_i64(const ds_cfg_node_t*, const char*, const bool, const int64_t) __attribute__((warn_unused_result));
const char* ds_cfg_node_get_str(const ds_cfg_node_t*, const char*) __attribute__((warn_unused_result));
const char* ds_cfg_node_get_self_str(ds_cfg_node_t*) __attribute__((warn_unused_result));
void ds_cfg_close(ds_cfg_t*);

