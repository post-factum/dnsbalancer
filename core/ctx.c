/* vim: set tabstop=4:softtabstop=4:shiftwidth=4:noexpandtab */

/*
 * dnsbalancer - daemon to balance UDP DNS requests over DNS servers
 * Initially created under patronage of Lanet Network
 * Programmed by Oleksandr Natalenko <oleksandr@natalenko.name>, 2015-2018
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <string.h>
#include <sys/timerfd.h>

#include "fe.h"
#include "fwd.h"
#include "utils.h"

#include "ctx.h"

struct ds_ctx* ds_ctx_load(const char* _config_file)
{
	struct ds_ctx* ret = NULL;
	ds_cfg_node_t* cfg_node_root = NULL;
	ds_cfg_node_t* cur_node_i = NULL;
	ds_cfg_node_t* cfg_node_j = NULL;
	ds_cfg_node_t* cur_node_j = NULL;
	ds_cfg_node_t* cur_node_k = NULL;
	ds_cfg_node_t* cfg_node_k = NULL;
	ds_cfg_node_t* cfg_node_general = NULL;
	__attribute__((unused)) ds_cfg_node_t* cfg_node_stats = NULL;
	ds_cfg_node_t* cfg_node_frontends = NULL;
	ds_cfg_node_t* cfg_node_actions = NULL;
	ds_cfg_node_t* cfg_node_forwarders = NULL;
	const char* cur_str_i = NULL;
	const char* cur_str_j = NULL;
	const char* cur_str_k = NULL;

	ret = pfcq_alloc(sizeof(struct ds_ctx));

	ret->cfg = ds_cfg_open(_config_file);

	cfg_node_root = ds_cfg_node_get_root(ret->cfg);

	cfg_node_general = ds_cfg_node_get_subnode(cfg_node_root, DS_CFG_KEY_GENERAL);
	cfg_node_stats = ds_cfg_node_get_subnode(cfg_node_root, DS_CFG_KEY_STATS);
	cfg_node_frontends = ds_cfg_node_get_subnode(cfg_node_root, DS_CFG_KEY_FRONTENDS);
	cfg_node_actions = ds_cfg_node_get_subnode(cfg_node_root, DS_CFG_KEY_ACTIONS);
	cfg_node_forwarders = ds_cfg_node_get_subnode(cfg_node_root, DS_CFG_KEY_FORWARDERS);

	pfcq_counter_init(&ret->epoch);
	ret->tk_fd = ds_timerfd_create(CLOCK_MONOTONIC, TFD_NONBLOCK);
	ret->epoch_size =
		ds_cfg_node_get_u64(cfg_node_general, DS_CFG_KEY_TK_INTVL,
			true, DS_CFG_DEFAULT_TK_INTVL) * 1000000ULL;
	ds_timerfd_settime(ret->tk_fd, ret->epoch_size);

	// forwarders
	ret->nfwds = ds_cfg_get_node_len(cfg_node_forwarders);
	ret->fwds = pfcq_alloc(ret->nfwds * sizeof(struct ds_fwd));
	for (size_t i = 0; i < ret->nfwds; i++)
	{
		cur_node_i = ds_cfg_get_node_elem(cfg_node_forwarders, i);

		// init
		pfcq_counter_init(&ret->fwds[i].c_q_id);
		pfcq_counter_set(&ret->fwds[i].c_q_id, 1);
		pfcq_counter_init(&ret->fwds[i].wdt_pending);

		// name
		ret->fwds[i].name = ds_cfg_node_get_str(cur_node_i, DS_CFG_KEY_NAME);

		// address
		ds_inet_pton(ds_cfg_node_get_str(cur_node_i, DS_CFG_KEY_ADDR),
			(in_port_t)ds_cfg_node_get_u64(cur_node_i, DS_CFG_KEY_PORT, true, DS_CFG_DEFAULT_DNS_PORT),
			&ret->fwds[i].addr);

		// regular DSCP
		ret->fwds[i].reg_dscp = ds_cfg_node_get_u64(cur_node_i, DS_CFG_KEY_DSCP, true, DS_CFG_DEFAULT_DSCP);

		// watchdog
		cur_node_j = ds_cfg_node_get_subnode(cur_node_i, DS_CFG_KEY_WDT);

		// watchdog DSCP
		ret->fwds[i].wdt_dscp = ds_cfg_node_get_u64(cur_node_j, DS_CFG_KEY_DSCP, true, DS_CFG_DEFAULT_DSCP);

		// watchdog query
		ret->fwds[i].wdt_query = ds_cfg_node_get_str(cur_node_j, DS_CFG_KEY_QUERY);

		// watchdog tries
		ret->fwds[i].wdt_tries = ds_cfg_node_get_u64(cur_node_j, DS_CFG_KEY_TRIES, true, DS_CFG_DEFAULT_TRIES);

		// forwarder is alive by default
		ret->fwds[i].alive = true;

		verbose("[fwd: %s] loaded\n", ret->fwds[i].name);
	}

	// actions
	ret->nacts = ds_cfg_get_node_len(cfg_node_actions);
	ret->acts = pfcq_alloc(ret->nacts * sizeof(struct ds_act));
	for (size_t i = 0; i < ret->nacts; i++)
	{
		cur_node_i = ds_cfg_get_node_elem(cfg_node_actions, i);

		// name
		ret->acts[i].name = ds_cfg_node_get_str(cur_node_i, DS_CFG_KEY_NAME);

		cfg_node_j = ds_cfg_node_get_subnode(cur_node_i, DS_CFG_KEY_RULES);
		ret->acts[i].nact_items = ds_cfg_get_node_len(cfg_node_j);
		ret->acts[i].act_items = pfcq_alloc(ret->acts[i].nact_items * sizeof(struct ds_act_item));

		for (size_t j = 0; j < ret->acts[i].nact_items; j++)
		{
			cur_node_j = ds_cfg_get_node_elem(cfg_node_j, j);

			// name
			ret->acts[i].act_items[j].name = ds_cfg_node_get_str(cur_node_j, DS_CFG_KEY_NAME);

			// address/mask
			ds_inet_pton(ds_cfg_node_get_str(cur_node_j, DS_CFG_KEY_NET),
				0, &ret->acts[i].act_items[j].addr);
			ds_inet_vlsmton(&ret->acts[i].act_items[j].addr,
				ds_cfg_node_get_str(cur_node_j, DS_CFG_KEY_MASK),
				&ret->acts[i].act_items[j].mask);

			// class/type
			ret->acts[i].act_items[j].rr_class =
				ds_pkt_get_rr_class_from_name(ds_cfg_node_get_str(cur_node_j, DS_CFG_KEY_CLASS));
			ret->acts[i].act_items[j].rr_type =
				ds_pkt_get_rr_type_from_name(ds_cfg_node_get_str(cur_node_j, DS_CFG_KEY_TYPE));

			// regex
			ret->acts[i].act_items[j].expr = ds_cfg_node_get_str(cur_node_j, DS_CFG_KEY_FQDN);
			ds_regcomp(&ret->acts[i].act_items[j].regex, ret->acts[i].act_items[j].expr);

			// action
			cur_node_k = ds_cfg_node_get_subnode(cur_node_j, DS_CFG_KEY_DO);
			cur_str_i = ds_cfg_node_get_str(cur_node_k, DS_CFG_KEY_WHAT);
			if (pfcq_strlcmp(cur_str_i, DS_CFG_KEY_BALANCE) == 0)
			{
				ret->acts[i].act_items[j].act.type = DS_ACT_BALANCE;

				cur_str_j = ds_cfg_node_get_str(cur_node_k, DS_CFG_KEY_HOW);

				if (pfcq_strlcmp(cur_str_j, DS_CFG_KEY_RR) == 0)
				{
					ret->acts[i].act_items[j].act.params.balance.type = DS_ACT_BALANCE_RR;
				} else if (pfcq_strlcmp(cur_str_j, DS_CFG_KEY_STICKY) == 0)
				{
					ret->acts[i].act_items[j].act.params.balance.type = DS_ACT_BALANCE_STICKY;
				} else
				{
					inform("Balancing type: %s\n", cur_str_j);
					stop("Unknown balancing type specified");
				}

				cfg_node_k = ds_cfg_node_get_subnode(cur_node_k, DS_CFG_KEY_WHERE);
				ret->acts[i].act_items[j].act.params.balance.nfwds = ds_cfg_get_node_len(cfg_node_k);
				ret->acts[i].act_items[j].act.params.balance.fwds =
					pfcq_alloc(ret->acts[i].act_items[j].act.params.balance.nfwds *
					sizeof(struct ds_fwd*));

				for (size_t k = 0; k < ret->acts[i].act_items[j].act.params.balance.nfwds; k++)
				{
					cur_str_k = ds_cfg_node_get_self_str(ds_cfg_get_node_elem(cfg_node_k, k));
					for (size_t t = 0; t < ret->nfwds; t++)
					{
						if (strncmp(cur_str_k, ret->fwds[t].name, strlen(cur_str_k)) == 0)
							ret->acts[i].act_items[j].act.params.balance.fwds[k] = &ret->fwds[t];
					}
				}

				pfcq_counter_init(&ret->acts[i].act_items[j].act.params.balance.c_fwd);
			} else if (pfcq_strlcmp(cur_str_i, DS_CFG_KEY_DROP) == 0)
			{
				ret->acts[i].act_items[j].act.type = DS_ACT_DROP;
			} else if (pfcq_strlcmp(cur_str_i, DS_CFG_KEY_REJECT) == 0)
			{
				ret->acts[i].act_items[j].act.type = DS_ACT_REJECT;

				cur_str_j = ds_cfg_node_get_str(cur_node_k, DS_CFG_KEY_WITH);

				if (pfcq_strlcmp(cur_str_j, DS_CFG_KEY_NXDOMAIN) == 0)
					ret->acts[i].act_items[j].act.params.reject.type = DS_ACT_REJECT_NXDOMAIN;
				else if (pfcq_strlcmp(cur_str_j, DS_CFG_KEY_SERVFAIL) == 0)
					ret->acts[i].act_items[j].act.params.reject.type = DS_ACT_REJECT_SERVFAIL;
				else
				{
					inform("Reject type: %s\n", cur_str_j);
					stop("Unknown reject type specified");
				}
			} else
			{
				inform("Action: %s\n", cur_str_i);
				stop("Unknown action specified");
			}
		}
		verbose("[act: %s] loaded\n", ret->acts[i].name);
	}

	ret->wdt_fd = ds_timerfd_create(CLOCK_MONOTONIC, TFD_NONBLOCK);
	ds_timerfd_settime(ret->wdt_fd,
		ds_cfg_node_get_u64(cfg_node_general, DS_CFG_KEY_WDT_INTVL,
			true, DS_CFG_DEFAULT_WDT_INTVL) * 1000000ULL);

	ret->nfes = ds_cfg_get_node_len(cfg_node_frontends);
	ret->fes = pfcq_alloc(ret->nfes * sizeof(struct ds_fe));
	for (size_t i = 0; i < ret->nfes; i++)
	{
		cur_node_i = ds_cfg_get_node_elem(cfg_node_frontends, i);

		// name
		ret->fes[i].name = ds_cfg_node_get_str(cur_node_i, DS_CFG_KEY_NAME);

		// address
		ds_inet_pton(ds_cfg_node_get_str(cur_node_i, DS_CFG_KEY_ADDR),
			(in_port_t)ds_cfg_node_get_u64(cur_node_i, DS_CFG_KEY_PORT, true, DS_CFG_DEFAULT_DNS_PORT),
			&ret->fes[i].addr);

		// DSCP
		ret->fes[i].dscp = ds_cfg_node_get_u64(cur_node_i, DS_CFG_KEY_DSCP, true, DS_CFG_DEFAULT_DSCP);

		// actions
		cfg_node_j = ds_cfg_node_get_subnode(cur_node_i, DS_CFG_KEY_ACTIONS);
		ret->fes[i].nacts = ds_cfg_get_node_len(cfg_node_j);
		ret->fes[i].acts = pfcq_alloc(ret->fes[i].nacts * sizeof(struct ds_act));
		for (size_t j = 0; j < ret->fes[i].nacts; j++)
		{
			cur_str_j = ds_cfg_node_get_self_str(ds_cfg_get_node_elem(cfg_node_j, j));
			for (size_t k = 0; k < ret->nacts; k++)
			{
				if (strncmp(cur_str_j, ret->acts[k].name, strlen(cur_str_j)) == 0)
					ret->fes[i].acts[j] = ret->acts[k];
			}
		}

		verbose("[fe: %s] loaded\n", ret->fes[i].name);
	}

	ret->max_pkt_size =
		ds_cfg_node_get_u64(cfg_node_general, DS_CFG_KEY_MAX_PKT_SIZE,
			true, DS_CFG_DEFAULT_MAX_PKT_SIZE);
	if (unlikely(ret->max_pkt_size > DS_PKT_MAX_LEN))
	{
		inform(DS_CFG_KEY_MAX_PKT_SIZE "=%zu exceeds max allowed packet size (%d)\n", ret->max_pkt_size, DS_PKT_MAX_LEN);
		stop("Stopping.");
	}

	pfcq_counter_init(&ret->in_flight);

	ret->poll_timeo =
		ds_cfg_node_get_u64(cfg_node_general, DS_CFG_KEY_POLL_TIMEO,
			true, DS_CFG_DEFAULT_POLL_TIMEO);
	ret->req_ttl =
		ds_cfg_node_get_u64(cfg_node_general, DS_CFG_KEY_REQ_TTL,
			true, DS_CFG_DEFAULT_REQ_TTL) * 1000000ULL;
	ret->gc_intvl =
		ds_cfg_node_get_u64(cfg_node_general, DS_CFG_KEY_GC_INTVL,
			true, DS_CFG_DEFAULT_GC_INTVL) * 1000000ULL;

	pfcq_counter_init(&ret->c_redirect_wrk);

	ret->nwrks = pfcq_hint_cpus(
		ds_cfg_node_get_i64(cfg_node_general, DS_CFG_KEY_WRKS,
			true, DS_CFG_DEFAULT_WRKS));
	ret->wrks = pfcq_alloc(ret->nwrks * sizeof(struct ds_wrk_ctx*));
	for (size_t i = 0; i < ret->nwrks; i++)
	{
		ret->wrks[i] = pfcq_alloc(sizeof(struct ds_wrk_ctx));

		ret->wrks[i]->ctx = ret;
		ret->wrks[i]->index = i;
		ret->wrks[i]->ready = ds_eventfd(0, 0);

		pthread_create(&ret->wrks[i]->id, NULL, ds_wrk_thread, (void*)ret->wrks[i]);
		ds_consume_u64(ret->wrks[i]->ready);
		ds_close(ret->wrks[i]->ready);
		verbose("[ctx: %p, wrk: %zu/%#lx] started\n", (void*)ret, ret->wrks[i]->index, (uintptr_t)ret->wrks[i]->id);
	}

	return ret;
}

void ds_ctx_unload(struct ds_ctx* _ctx)
{
	for (size_t i = 0; i < _ctx->nwrks; i++)
		ds_produce_u64(_ctx->wrks[i]->ev_exit_fd);

	pfcq_counter_done(&_ctx->c_redirect_wrk);

	for (size_t i = 0; i < _ctx->nwrks; i++)
	{
		pthread_join(_ctx->wrks[i]->id, NULL);
		verbose("[ctx: %p, wrk: %zu/%#lx] exited\n", (void*)_ctx, _ctx->wrks[i]->index, (uintptr_t)_ctx->wrks[i]->id);
		pfcq_free(_ctx->wrks[i]);
	}
	pfcq_free(_ctx->wrks);

	ds_close(_ctx->wdt_fd);

	pfcq_counter_done(&_ctx->in_flight);

	for (size_t i = 0; i < _ctx->nfes; i++)
	{
		pfcq_free(_ctx->fes[i].acts);
	}
	pfcq_free(_ctx->fes);

	for (size_t i = 0; i < _ctx->nacts; i++)
	{
		for (size_t j = 0; j < _ctx->acts[i].nact_items; j++)
		{
			regfree(&_ctx->acts[i].act_items[j].regex);
			if (_ctx->acts[i].act_items[j].act.type == DS_ACT_BALANCE &&
				_ctx->acts[i].act_items[j].act.params.balance.nfwds != 0)
				pfcq_free(_ctx->acts[i].act_items[j].act.params.balance.fwds);
		}
		pfcq_free(_ctx->acts[i].act_items);
	}
	pfcq_free(_ctx->acts);

	for (size_t i = 0; i < _ctx->nfwds; i++)
	{
		pfcq_counter_done(&_ctx->fwds[i].c_q_id);
		pfcq_counter_done(&_ctx->fwds[i].wdt_pending);
	}
	pfcq_free(_ctx->fwds);

	ds_close(_ctx->tk_fd);
	pfcq_counter_done(&_ctx->epoch);

	ds_cfg_close(_ctx->cfg);

	pfcq_free(_ctx);
}

