/* vim: set tabstop=4:softtabstop=4:shiftwidth=4:noexpandtab */

/*
 * dnsbalancer - daemon to balance UDP DNS requests over DNS servers
 * Initially created under patronage of Lanet Network
 * Programmed by Oleksandr Natalenko <oleksandr@natalenko.name>, 2015-2018
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "act.h"
#include "cfg.h"
#include "../contrib/pfcq/pfcq.h"

struct ds_ctx
{
	ds_cfg_t* cfg;
	bool redirect;
	struct pfcq_counter c_redirect_wrk;
	struct ds_ctx* ctx_next;
	size_t max_pkt_size;
	struct ds_fe* fes;
	size_t nfes;
	struct ds_fwd* fwds;
	size_t nfwds;
	struct ds_act* acts;
	size_t nacts;
	int wdt_fd;
	int tk_fd;
	uint64_t req_ttl;
	uint64_t gc_intvl;
	struct ds_wrk_ctx** wrks;
	size_t nwrks;
	int poll_timeo;
	struct pfcq_counter epoch;
	uint64_t epoch_size;
	struct pfcq_counter in_flight;
};

struct ds_ctx* ds_ctx_load(const char*) __attribute__((warn_unused_result));
void ds_ctx_unload(struct ds_ctx*);

