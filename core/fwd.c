/* vim: set tabstop=4:softtabstop=4:shiftwidth=4:noexpandtab */

/*
 * dnsbalancer - daemon to balance UDP DNS requests over DNS servers
 * Initially created under patronage of Lanet Network
 * Programmed by Oleksandr Natalenko <oleksandr@natalenko.name>, 2015-2018
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "utils.h"

#include "fwd.h"

void ds_fwd_get_next(struct ds_wrk_tsk* _tsk, struct rb_table* _fwd_sk_set, struct ds_act_item* _act_item)
{
	struct ds_fwd* fwd = NULL;
	struct rb_traverser iter;
	struct ds_fwd_sk* cur_fwd_sk = NULL;
	size_t tries = 0;
	size_t tip = 0;

	switch (_act_item->act.params.balance.type)
	{
		case DS_ACT_BALANCE_RR:
			while (likely(tries++ < _act_item->act.params.balance.nfwds))
			{
				int index = pfcq_counter_get_inc_mod(&_act_item->act.params.balance.c_fwd,
											   _act_item->act.params.balance.nfwds, 0);
				fwd = _act_item->act.params.balance.fwds[index];
				if (likely(fwd->alive))
					break;
			}
			break;
		case DS_ACT_BALANCE_STICKY:
			tip = (size_t)(ds_hash_address(&_tsk->addr) % _act_item->act.params.balance.nfwds);
			while (likely(tries++ < _act_item->act.params.balance.nfwds))
			{
				int index = tip++ % _act_item->act.params.balance.nfwds;
				fwd = _act_item->act.params.balance.fwds[index];
				if (likely(fwd->alive))
					break;
			}
			break;
		default:
			panic("Unknown forwarding mode");
			break;
	}

	if (unlikely(!fwd))
		return;

	rb_t_init(&iter, _fwd_sk_set);
	cur_fwd_sk = rb_t_first(&iter, _fwd_sk_set);
	do {
		if (cur_fwd_sk->fwd == fwd)
		{
			_tsk->fwd_sk = cur_fwd_sk;
			break;
		}
	} while (likely((cur_fwd_sk = rb_t_next(&iter)) != NULL));

	if (unlikely(!_tsk->fwd_sk))
		return;

	_tsk->fwd = fwd;
	_tsk->fwd_sk_addr = _tsk->fwd_sk->fwd->addr;

	return;
}

int ds_fwd_sk_cmp(const void* _p1, const void* _p2, void* _param)
{
	(void)_param;

	const struct ds_fwd_sk* s1 = _p1;
	const struct ds_fwd_sk* s2 = _p2;

	return ds_int_cmp(s1->sk, s2->sk);
}

