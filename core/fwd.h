/* vim: set tabstop=4:softtabstop=4:shiftwidth=4:noexpandtab */

/*
 * dnsbalancer - daemon to balance UDP DNS requests over DNS servers
 * Initially created under patronage of Lanet Network
 * Programmed by Oleksandr Natalenko <oleksandr@natalenko.name>, 2015-2018
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "../contrib/libavl/rb.h"
#include "act.h"
#include "wrk.h"

struct ds_fwd
{
	const char* name;					// ] meta

	struct pfcq_net_addr addr;			// ]
	int reg_dscp;						// | net
	int wdt_dscp;						// ]

	struct pfcq_counter c_q_id;			// ] DNS query ID

	const char* wdt_query;				// ]
	struct pfcq_counter wdt_pending;	// | watchdog
	size_t wdt_tries;					// |
	bool alive;							// ]
};

struct ds_fwd_sk
{
	int sk;
	struct ds_fwd* fwd;
};

void ds_fwd_get_next(struct ds_wrk_tsk*, struct rb_table*, struct ds_act_item*);
int ds_fwd_sk_cmp(const void*, const void*, void*) __attribute__((warn_unused_result));

