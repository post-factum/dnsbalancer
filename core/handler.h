/* vim: set tabstop=4:softtabstop=4:shiftwidth=4:noexpandtab */

/*
 * dnsbalancer - daemon to balance UDP DNS requests over DNS servers
 * Initially created under patronage of Lanet Network
 * Programmed by Oleksandr Natalenko <oleksandr@natalenko.name>, 2015-2018
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

int ds_handler_acpt(struct ds_fe_sk*, struct ds_wrk_ctx*) __attribute__((warn_unused_result));
int ds_handler_prep(int, struct ds_wrk_ctx*) __attribute__((warn_unused_result));
int ds_handler_fwd(int, struct ds_wrk_ctx*) __attribute__((warn_unused_result));
int ds_handler_rej(int, struct ds_wrk_ctx*) __attribute__((warn_unused_result));
int ds_handler_obt(struct ds_fwd_sk*, struct ds_wrk_ctx*) __attribute__((warn_unused_result));
int ds_handler_rep(int, struct ds_wrk_ctx*) __attribute__((warn_unused_result));
int ds_handler_exit(int, struct ds_wrk_ctx*) __attribute__((warn_unused_result));
int ds_handler_gc(int, struct ds_wrk_ctx*) __attribute__((warn_unused_result));
int ds_handler_wdt_req(int, struct ds_wrk_ctx*) __attribute__((warn_unused_result));
int ds_handler_wdt_rep(int, struct ds_wrk_ctx*) __attribute__((warn_unused_result));
int ds_handler_tk(int, struct ds_wrk_ctx*) __attribute__((warn_unused_result));

