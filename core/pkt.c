/* vim: set tabstop=4:softtabstop=4:shiftwidth=4:noexpandtab */

/*
 * dnsbalancer - daemon to balance UDP DNS requests over DNS servers
 * Initially created under patronage of Lanet Network
 * Programmed by Oleksandr Natalenko <oleksandr@natalenko.name>, 2015-2018
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../contrib/pfcq/pfcq.h"

#include "pkt.h"

uint16_t ds_pkt_get_id(struct ds_pkt* _pkt)
{
	return ldns_pkt_id(_pkt->lp);
}

ds_rr_class_t ds_pkt_get_q_class(const struct ds_pkt* _pkt)
{
	return ldns_rr_get_class(ldns_rr_list_rr(ldns_pkt_question(_pkt->lp), 0));
}

ds_rr_type_t ds_pkt_get_q_type(const struct ds_pkt* _pkt)
{
	return ldns_rr_get_type(ldns_rr_list_rr(ldns_pkt_question(_pkt->lp), 0));
}

static char* ds_pkt_get_fqdn(struct ds_pkt* _pkt)
{
	ldns_rdf* owner = NULL;
	owner = ldns_rr_owner(ldns_rr_list_rr(ldns_pkt_question(_pkt->lp), 0));
	ldns_dname2canonical(owner);
	return ldns_rdf2str(owner);
}

int ds_pkt_from_raw(char* _data, const size_t _data_len, struct ds_pkt* _pkt)
{
	if (unlikely(ldns_wire2pkt(&_pkt->lp, (const uint8_t*)_data, _data_len) != LDNS_STATUS_OK))
		return -1;

	_pkt->fqdn = ds_pkt_get_fqdn(_pkt);

	return 0;
}

int ds_pkt_craft_query(const char* _query, struct ds_pkt* _pkt, char** _buf, size_t* _buf_len)
{
	ldns_rr* rr = NULL;
	int push_res = -1;

	_pkt->lp = ldns_pkt_new();
	if (unlikely(!_pkt->lp))
		goto err;

	ldns_pkt_set_random_id(_pkt->lp);
	ldns_pkt_set_qr(_pkt->lp, 0);
	ldns_pkt_set_opcode(_pkt->lp, LDNS_PACKET_QUERY);
	ldns_pkt_set_tc(_pkt->lp, 0);
	ldns_pkt_set_rd(_pkt->lp, 1);
	if (unlikely(ldns_rr_new_question_frm_str(&rr, _query, NULL, NULL) != LDNS_STATUS_OK))
		goto err_free_pkt;
	push_res = ldns_pkt_push_rr(_pkt->lp, LDNS_SECTION_QUESTION, rr);
	if (unlikely(push_res != LDNS_STATUS_OK && push_res != LDNS_STATUS_EMPTY_LABEL))
		goto err_free_pkt;

	if (unlikely(ldns_pkt2wire((uint8_t**)_buf, _pkt->lp, _buf_len) != LDNS_STATUS_OK))
		goto err_free_pkt;

	_pkt->fqdn = ds_pkt_get_fqdn(_pkt);

	goto out;

err_free_pkt:
	ldns_pkt_free(_pkt->lp);

err:
	return -1;

out:
	return 0;
}

ldns_rr_class ds_pkt_get_rr_class_from_name(const char* _name)
{
	return ldns_get_rr_class_by_name(_name);
}

ldns_rr_type ds_pkt_get_rr_type_from_name(const char* _name)
{
	return ldns_get_rr_type_by_name(_name);
}

void ds_pkt_free(struct ds_pkt* _pkt)
{
	// allocated by LDNS, thus using free()
	if (likely(_pkt->fqdn))
		free(_pkt->fqdn);

	if (likely(_pkt->lp))
		ldns_pkt_free(_pkt->lp);

	return;
}

