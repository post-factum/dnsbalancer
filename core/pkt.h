/* vim: set tabstop=4:softtabstop=4:shiftwidth=4:noexpandtab */

/*
 * dnsbalancer - daemon to balance UDP DNS requests over DNS servers
 * Initially created under patronage of Lanet Network
 * Programmed by Oleksandr Natalenko <oleksandr@natalenko.name>, 2015-2018
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <stdint.h>

#include "../contrib/ldns/ldns/ldns.h"

#define DS_PKT_MAX_LEN	LDNS_MAX_PACKETLEN

#define DS_RR_CLASS_ALL	0
#define DS_RR_TYPE_ALL	0

enum ds_pkt_type
{
	DS_PKT_UNK = 0,
	DS_PKT_REQ,
	DS_PKT_REP,
};

struct ds_pkt
{
	ldns_pkt* lp;
	char* fqdn;
};

typedef ldns_rr_class ds_rr_class_t;
typedef ldns_rr_type ds_rr_type_t;

uint16_t ds_pkt_get_id(struct ds_pkt*) __attribute__((warn_unused_result));
ds_rr_class_t ds_pkt_get_q_class(const struct ds_pkt*) __attribute__((warn_unused_result));
ds_rr_type_t ds_pkt_get_q_type(const struct ds_pkt*) __attribute__((warn_unused_result));
int ds_pkt_from_raw(char*, const size_t, struct ds_pkt*) __attribute__((warn_unused_result));
int ds_pkt_craft_query(const char*, struct ds_pkt*, char**, size_t*) __attribute__((warn_unused_result));
ldns_rr_class ds_pkt_get_rr_class_from_name(const char*) __attribute__((warn_unused_result));
ldns_rr_type ds_pkt_get_rr_type_from_name(const char*) __attribute__((warn_unused_result));
void ds_pkt_free(struct ds_pkt* _pkt);

