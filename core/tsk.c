/* vim: set tabstop=4:softtabstop=4:shiftwidth=4:noexpandtab */

/*
 * dnsbalancer - daemon to balance UDP DNS requests over DNS servers
 * Initially created under patronage of Lanet Network
 * Programmed by Oleksandr Natalenko <oleksandr@natalenko.name>, 2015-2018
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <sys/param.h>
#include <string.h>

#include "ctx.h"
#include "fwd.h"
#include "tsk.h"
#include "utils.h"

int ds_tsk_buf_parse(struct ds_wrk_ctx* _wrk_ctx, struct ds_wrk_tsk* _tsk, enum ds_pkt_type _pkt_type)
{
	struct ds_fwd* fwd = NULL;

	switch (_pkt_type)
	{
		case DS_PKT_REQ:
			/* network endianness is used */
			_tsk->orig_id = htons(ds_pkt_get_id(&_tsk->pkt));
			switch (_tsk->type)
			{
				case DS_TSK_REG:
					fwd = _tsk->fwd;
					break;
				case DS_TSK_WDT:
					fwd = _tsk->fwd_sk->fwd;
					break;
				default:
					panic("Unknown task type");
					break;
			}
			_tsk->subst_id = htons(pfcq_counter_get_inc_mod(&fwd->c_q_id, UINT16_MAX + 1, 1));
			memcpy(_tsk->buf, &_tsk->subst_id, sizeof(uint16_t));
			pfcq_counter_set(&_tsk->epoch, pfcq_counter_get(&_wrk_ctx->ctx->epoch));
			break;
		case DS_PKT_REP:
			/* network endianness is used */
			_tsk->subst_id = htons(ds_pkt_get_id(&_tsk->pkt));
			break;
		default:
			panic("Unknown packet type");
			break;
	}

	return 0;
}

int ds_tsk_cmp(const void* _p1, const void* _p2, void* _param)
{
	(void)_param;

	const struct ds_wrk_tsk* t1 = _p1;
	const struct ds_wrk_tsk* t2 = _p2;

	int _s0 = ds_int_cmp(t1->subst_id, t2->subst_id);
	if (_s0 == 0)
	{
		int _s1 = strcmp(t1->pkt.fqdn, t2->pkt.fqdn);
		if (_s1 == 0)
		{
			int _s2 = ds_int_cmp(ds_pkt_get_q_type(&t1->pkt), ds_pkt_get_q_type(&t2->pkt));
			if (_s2 == 0)
			{
				int _s3 = ds_int_cmp(ds_pkt_get_q_class(&t1->pkt), ds_pkt_get_q_class(&t2->pkt));
				if (_s3 == 0)
				{
					return ds_ptr_cmp(t1->fwd_sk->fwd, t2->fwd_sk->fwd);
				} else
					return _s3;
			} else
				return _s2;
		} else
			return _s1;
	} else
		return _s0;
}

void ds_tsk_free(struct ds_wrk_tsk* _tsk)
{
	pfcq_free(_tsk->buf);
	ds_pkt_free(&_tsk->pkt);
	pfcq_counter_done(&_tsk->epoch);
	pfcq_free(_tsk);

	return;
}

