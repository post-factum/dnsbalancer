/* vim: set tabstop=4:softtabstop=4:shiftwidth=4:noexpandtab */

/*
 * dnsbalancer - daemon to balance UDP DNS requests over DNS servers
 * Initially created under patronage of Lanet Network
 * Programmed by Oleksandr Natalenko <oleksandr@natalenko.name>, 2015-2018
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <limits.h>
#include <pthread.h>
#include <stdbool.h>

#include "../contrib/pfcq/pfcq.h"
#include "../contrib/queue.h"
#include "pkt.h"

enum ds_wrk_tsk_type
{
	DS_TSK_UNK = 0,
	DS_TSK_REG,
	DS_TSK_WDT,
};

struct ds_wrk_tsk
{
	TAILQ_ENTRY(ds_wrk_tsk) tailq;

	bool redirected;					// ] meta

	char* buf;							// ]
	ssize_t buf_size;					// | raw
	struct ds_pkt pkt;					// ]

	uint16_t subst_id;					// ]
	struct ds_fwd_sk* fwd_sk;			// | key
	struct pfcq_net_addr fwd_sk_addr;	// ]

	struct pfcq_net_addr addr;			// ]
	uint16_t orig_id;					// |
	struct ds_fe_sk* orig_fe_sk;		// |
	struct pfcq_net_addr orig_fe_addr;	// | value
	struct ds_fwd* fwd;					// |
	struct pfcq_counter epoch;			// |
	enum ds_wrk_tsk_type type;			// ]
};

TAILQ_HEAD(ds_wrk_tsk_list, ds_wrk_tsk);

struct ds_wrk_ctx
{
	struct ds_ctx* ctx;					// ]
	size_t index;						// | meta
	pthread_t id;						// ]

	int ready;

	struct rb_table* tracking;
	struct rb_table* fe_sk_set;
	struct rb_table* fwd_sk_set;
	struct rb_table* fwd_wdt_sk_set;
	int poll_timeo;
	int wrk_fd;

	int ev_prep_fd;
	int ev_fwd_fd;
	int ev_rej_fd;
	int ev_rep_fd;
	int ev_wdt_rep_fd;
	int ev_gc_fd;
	int ev_exit_fd;
	struct ds_wrk_tsk_list prep_queue;
	struct ds_wrk_tsk_list fwd_queue;
	struct ds_wrk_tsk_list rej_queue;
	struct ds_wrk_tsk_list rep_queue;
	pthread_spinlock_t rep_queue_lock;
	struct ds_wrk_tsk_list wdt_rep_queue;
	pthread_spinlock_t wdt_rep_queue_lock;
};

void* ds_wrk_thread(void*);

